FROM openjdk
RUN mvn clean verify
COPY /target/userservice-0.0.1-SNAPSHOT.jar ./app.jar
EXPOSE 8080
RUN adduser -D user
USER user
CMD [ "sh", "-c", "java -Dserver.port=$PORT -jar app.jar" ]