package bvnk3r.provinggrounds.userservice.model.rest;

import lombok.Data;

import java.util.ArrayList;
import java.util.Collection;

@Data
public class CreateUserRequest {

    private String name;
    private String username;
    private String password;
    private Collection<String> roles = new ArrayList<>();
}
