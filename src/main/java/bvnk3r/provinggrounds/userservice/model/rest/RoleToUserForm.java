package bvnk3r.provinggrounds.userservice.model.rest;

import lombok.Data;

@Data
public class RoleToUserForm {
    private String username;
    private String roleName;
}