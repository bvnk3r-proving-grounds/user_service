package bvnk3r.provinggrounds.userservice.model.rest;

import lombok.Data;

@Data
public class RoleResponse {

    private Long id;
    private String name;
}
