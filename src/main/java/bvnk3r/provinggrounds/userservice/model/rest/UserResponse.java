package bvnk3r.provinggrounds.userservice.model.rest;

import lombok.Data;

import java.util.ArrayList;
import java.util.Collection;

@Data
public class UserResponse {

    private Long id;
    private String name;
    private String username;
    private Collection<String> roles = new ArrayList<>();
}
