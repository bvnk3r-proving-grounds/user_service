package bvnk3r.provinggrounds.userservice.model.rest;

import lombok.Data;

@Data
public class CreateRoleRequest {

    private String name;
}
