package bvnk3r.provinggrounds.userservice.mapper;

import bvnk3r.provinggrounds.userservice.model.Role;
import bvnk3r.provinggrounds.userservice.model.User;
import bvnk3r.provinggrounds.userservice.model.rest.UserResponse;
import bvnk3r.provinggrounds.userservice.repository.UserRepository;
import org.mapstruct.AfterMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.stream.Collectors;

@Mapper(componentModel = "spring")
public abstract class UserResponseMapper {

    @Mapping(target = "roles", ignore = true)
    public abstract UserResponse toUserResponse(User user);

    @Mapping(target = "roles", ignore = true)
    public abstract List<UserResponse> toUserResponse(List<User> users);

    @AfterMapping
    protected void afterToResourceResponse(User user, @MappingTarget UserResponse response) {
        response.setRoles(user.getRoles().stream().map(Role::getName).collect(Collectors.toSet()));
    }

    @AfterMapping
    protected void afterToResourceResponse(List<User> users, @MappingTarget UserResponse response) {
        for(User user: users) {
            response.setRoles(user.getRoles().stream().map(Role::getName).collect(Collectors.toSet()));
        }
    }
}
