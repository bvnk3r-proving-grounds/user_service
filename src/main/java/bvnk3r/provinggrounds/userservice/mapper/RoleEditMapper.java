package bvnk3r.provinggrounds.userservice.mapper;

import bvnk3r.provinggrounds.userservice.model.Role;
import bvnk3r.provinggrounds.userservice.model.User;
import bvnk3r.provinggrounds.userservice.model.rest.CreateRoleRequest;
import bvnk3r.provinggrounds.userservice.model.rest.CreateUserRequest;
import bvnk3r.provinggrounds.userservice.repository.RoleRepository;
import lombok.RequiredArgsConstructor;
import org.mapstruct.AfterMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;

import static java.util.stream.Collectors.toSet;

@Mapper(componentModel = "spring")
@RequiredArgsConstructor
public abstract class RoleEditMapper {

    @Mapping(target = "createdAt", ignore = true)
    @Mapping(target = "modifiedAt", ignore = true)
    public abstract Role create(CreateRoleRequest request);
}
