package bvnk3r.provinggrounds.userservice.mapper;

import bvnk3r.provinggrounds.userservice.model.Role;
import bvnk3r.provinggrounds.userservice.model.User;
import bvnk3r.provinggrounds.userservice.model.rest.RoleResponse;
import bvnk3r.provinggrounds.userservice.model.rest.UserResponse;
import bvnk3r.provinggrounds.userservice.repository.RoleRepository;
import bvnk3r.provinggrounds.userservice.repository.UserRepository;
import org.mapstruct.Mapper;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

@Mapper(componentModel = "spring")
public abstract class RoleResponseMapper {

    public abstract RoleResponse toRoleResponse(Role role);
}
