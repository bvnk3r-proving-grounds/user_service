package bvnk3r.provinggrounds.userservice.mapper;

import bvnk3r.provinggrounds.userservice.model.User;
import bvnk3r.provinggrounds.userservice.model.rest.CreateUserRequest;
import bvnk3r.provinggrounds.userservice.repository.RoleRepository;
import lombok.RequiredArgsConstructor;
import org.mapstruct.AfterMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.springframework.beans.factory.annotation.Autowired;

import static java.util.stream.Collectors.toSet;

@Mapper(componentModel = "spring")
public abstract class UserEditMapper {

    @Autowired
    private RoleRepository roleRepository;

    @Mapping(target = "roles", ignore = true)
    @Mapping(target = "createdAt", ignore = true)
    @Mapping(target = "modifiedAt", ignore = true)
    public abstract User create(CreateUserRequest request);

    @AfterMapping
    protected void afterCreate(CreateUserRequest request, @MappingTarget User user) {
        if (request.getRoles() != null) {
            user.setRoles(request.getRoles().stream().map(roleRepository::findByName).collect(toSet()));
        }
    }
}
