package bvnk3r.provinggrounds.userservice;

import bvnk3r.provinggrounds.userservice.model.Role;
import bvnk3r.provinggrounds.userservice.model.User;
import bvnk3r.provinggrounds.userservice.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class Demo implements ApplicationListener<ApplicationReadyEvent> {

    private final UserService userService;

    @Override
    public void onApplicationEvent(ApplicationReadyEvent applicationReadyEvent) {

        userService.saveRole(new Role("ROLE_USER"));
        userService.saveRole(new Role("ROLE_MANAGER"));
        userService.saveRole(new Role("ROLE_ADMIN"));
        userService.saveRole(new Role("ROLE_SUPER_ADMIN"));

        userService.saveUser(new User("John Travolta", "john", "1234"));
        userService.saveUser(new User("Will Smith", "will", "1234"));
        userService.saveUser(new User("Jim Carry", "jim", "1234"));
        userService.saveUser(new User("Arnold Schwarzenegger", "arnold", "1234"));

        userService.addRoleToUser("john", "ROLE_USER");
        userService.addRoleToUser("will", "ROLE_MANAGER");
        userService.addRoleToUser("jim", "ROLE_ADMIN");
        userService.addRoleToUser("arnold", "ROLE_SUPER_ADMIN");
        userService.addRoleToUser("arnold", "ROLE_ADMIN");
        userService.addRoleToUser("arnold", "ROLE_USER");
    }
}
