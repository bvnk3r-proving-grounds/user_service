package bvnk3r.provinggrounds.userservice.controller;

import bvnk3r.provinggrounds.userservice.mapper.RoleEditMapper;
import bvnk3r.provinggrounds.userservice.mapper.RoleResponseMapper;
import bvnk3r.provinggrounds.userservice.mapper.UserEditMapper;
import bvnk3r.provinggrounds.userservice.mapper.UserResponseMapper;
import bvnk3r.provinggrounds.userservice.model.Role;
import bvnk3r.provinggrounds.userservice.model.User;
import bvnk3r.provinggrounds.userservice.model.rest.*;
import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.fasterxml.jackson.databind.ObjectMapper;
import bvnk3r.provinggrounds.userservice.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.MimeTypeUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.net.URI;
import java.util.*;
import java.util.stream.Collectors;

import static org.springframework.http.HttpHeaders.AUTHORIZATION;
import static org.springframework.http.HttpStatus.FORBIDDEN;

@RestController
@RequestMapping("/api")
@RequiredArgsConstructor
public class UserController {

    private final UserService userService;
    private final UserEditMapper userEditMapper;
    private final UserResponseMapper userResponseMapper;
    private final RoleEditMapper roleEditMapper;
    private final RoleResponseMapper roleResponseMapper;

    @GetMapping("/users")
    public ResponseEntity<List<UserResponse>>getUsers() {
        return ResponseEntity.ok().body(userResponseMapper.toUserResponse(userService.getUsers()));
    }

    @PostMapping("/user/save")
    public ResponseEntity<UserResponse>saveUser(@RequestBody CreateUserRequest request) {
        URI uri = URI.create(ServletUriComponentsBuilder.fromCurrentContextPath().path("/api/user/save").toUriString());
        User user = userService.saveUser(userEditMapper.create(request));
        return ResponseEntity.created(uri).body(userResponseMapper.toUserResponse(user));
    }

    @PostMapping("/role/save")
    public ResponseEntity<RoleResponse>saveRole(@RequestBody CreateRoleRequest request) {
        URI uri = URI.create(ServletUriComponentsBuilder.fromCurrentContextPath().path("/api/role/save").toUriString());
        Role role = userService.saveRole(roleEditMapper.create(request));
        return ResponseEntity.created(uri).body(roleResponseMapper.toRoleResponse(role));
    }

    @PostMapping("/role/addtouser")
    public ResponseEntity<?>addRoleToUser(@RequestBody RoleToUserForm form) {
        userService.addRoleToUser(form.getUsername(), form.getRoleName());
        return ResponseEntity.ok().build();
    }

    @GetMapping("/token/refresh")
    public void refreshToken(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String authorizationHeader = request.getHeader(AUTHORIZATION);
        if(authorizationHeader != null && authorizationHeader.startsWith("Bearer ")) {
            try {
                String refresh_token = authorizationHeader.substring("Bearer ".length());
                Algorithm algorithm = Algorithm.HMAC256("jwtSecret".getBytes());
                JWTVerifier verifier = JWT.require(algorithm).build();
                DecodedJWT decodedJWT = verifier.verify(refresh_token);
                String username = decodedJWT.getSubject();
                User user = userService.getUser(username);
                String access_token = JWT.create()
                        .withSubject(user.getUsername())
                        .withExpiresAt(new Date(System.currentTimeMillis() + 10 * 60 * 1000))
                        .withIssuer(request.getRequestURL().toString())
                        .withClaim("roles", user.getRoles().stream().map(Role::getName).collect(Collectors.toList()))
                        .sign(algorithm);
                Map<String, String> tokens = new HashMap<>();
                tokens.put("access_token", access_token);
                tokens.put("refresh_token", refresh_token);
                response.setContentType(MediaType.APPLICATION_JSON_VALUE);
                new ObjectMapper().writeValue(response.getOutputStream(), tokens);
            }catch (Exception exception) {
                response.setHeader("error", exception.getMessage());
                response.setStatus(FORBIDDEN.value());
                Map<String, String> error = new HashMap<>();
                error.put("error_message", exception.getMessage());
                response.setContentType(MimeTypeUtils.APPLICATION_JSON_VALUE);
                new ObjectMapper().writeValue(response.getOutputStream(), error);
            }
        } else {
            throw new RuntimeException("Refresh token is missing");
        }
    }
}

