create sequence hibernate_sequence start with 1 increment by 1;

create table role (
    id bigint not null,
    created_at timestamp,
    modified_at timestamp,
    name varchar(255),
    primary key (id)
);

create table usr (
    id bigint not null,
    created_at timestamp,
    modified_at timestamp,
    name varchar(255),
    password varchar(255),
    username varchar(255),
    primary key (id)
);

create table usr_roles (
    user_id bigint not null,
    roles_id bigint not null
);

alter table if exists role
    add constraint role_name_unique unique (name);

alter table if exists usr
    add constraint usr_username_unique unique (username);

alter table usr_roles
    add constraint user_roles_role_fk
    foreign key (roles_id) references role;

alter table usr_roles
    add constraint user_roles_user_fk
    foreign key (user_id) references usr;