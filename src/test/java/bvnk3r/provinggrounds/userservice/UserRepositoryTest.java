package bvnk3r.provinggrounds.userservice;

import bvnk3r.provinggrounds.userservice.model.User;
import bvnk3r.provinggrounds.userservice.repository.UserRepository;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import static org.assertj.core.api.Assertions.assertThat;

@DataJpaTest
public class UserRepositoryTest {

    @Autowired
    private UserRepository userRepository;

    @BeforeEach
    void beforeEach() {
        User user = new User("Test user", "username", "password");
        userRepository.save(user);
    }

    @AfterEach
    void afterEach() {
        userRepository.deleteAll();
    }

    @Test
    void findByNameTest() {
        User user = userRepository.findByUsername("username");
        assertThat(user).isNotNull();
        assertThat(user.getName()).isEqualTo("Test user");
        assertThat(user.getPassword()).isEqualTo("password");
    }
}
