package bvnk3r.provinggrounds.userservice;

import bvnk3r.provinggrounds.userservice.model.Role;
import bvnk3r.provinggrounds.userservice.model.User;
import bvnk3r.provinggrounds.userservice.repository.RoleRepository;
import bvnk3r.provinggrounds.userservice.repository.UserRepository;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import static org.assertj.core.api.Assertions.assertThat;

@DataJpaTest
public class RoleRepositoryTest {

    @Autowired
    private RoleRepository roleRepository;

    @BeforeEach
    void beforeEach() {
        Role role = new Role("ROLE_ADMIN");
        roleRepository.save(role);
    }

    @AfterEach
    void afterEach() {
        roleRepository.deleteAll();
    }

    @Test
    void findByNameTest() {
        Role role = roleRepository.findByName("ROLE_ADMIN");
        assertThat(role).isNotNull();
        assertThat(role.getName()).isEqualTo("ROLE_ADMIN");
    }
}
